package data;

import android.text.format.DateUtils;
import com.android.medisafe.android_interview_tests.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Mendi on 15/10/2018.
 */

public class DatebaseMock {

    public static List<MedItem> getAllMedItems() {
        List<MedItem> items = new ArrayList<>();
        for (int i = 1; i < 200; i++) {
            String name = generateName();
            items.add(new MedItem(name, i, generateImage(name),
                    System.currentTimeMillis() + (i * DateUtils.HOUR_IN_MILLIS), generateStatus(),
                    "100 mg"));
        }

        return items;
    }

    private static MedItem.Status generateStatus() {
        MedItem.Status[] statuses = {MedItem.Status.MISSED, MedItem.Status.PENDING
                , MedItem.Status.SNOOZE, MedItem.Status.TAKEN};
        return statuses[new Random().nextInt(4)];
    }

    private static int generateImage(String name) {
        switch (name) {
            case "novaring":
                return R.drawable.nuvaring_insert_white;
            case "Advil":
                return R.drawable.oval_white;
            default:
                return R.drawable.round_white;
        }

    }

    private static String generateName() {
        String[] names = {"Advil", "Ninlaro", "doxazosin", "novaring"};
        return names[new Random().nextInt(3)];
    }

}
