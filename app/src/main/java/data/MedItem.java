package data;

/**
 * Created by Mendi on 15/10/2018.
 */

public class MedItem {

    private String name;
    private int id;
    private int shapeImageResource;
    private long time;
    private Status status;
    private String  dose;

    public MedItem(String name, int id, int shapeImageResource, long time, Status status, String dose) {
        this.name = name;
        this.id = id;
        this.shapeImageResource = shapeImageResource;
        this.time = time;
        this.status = status;
        this.dose = dose;
    }

    enum Status{
        TAKEN, SNOOZE, PENDING, MISSED
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getShapeImageResource() {
        return shapeImageResource;
    }

    public long getTime() {
        return time;
    }

    public Status getStatus() {
        return status;
    }

    public String getDose() {
        return dose;
    }
}
