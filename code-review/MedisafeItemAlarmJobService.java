package com.medisafe.android.base.alarms;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.os.Build;

import com.medisafe.android.base.helpers.EventsHelper;
import com.medisafe.common.Mlog;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.medisafe.android.base.alarms.MedisafeItemAlarmManager.startJobServicePeriodic;

@TargetApi(Build.VERSION_CODES.N)
public class MedisafeItemAlarmJobService extends JobService {

    private static final String TAG = MedisafeItemAlarmJobService.class.getSimpleName();
    public static final int PERIODIC_JOB_ID = 398674;
    public static final int ONE_TIME_JOB_ID = 978467;
    public static final String EXTRA_IS_PERIODIC = "EXTRA_IS_PERIODIC";
    private boolean mIsPeriodic;


    @Override
    public boolean onStartJob(JobParameters params) {
        Mlog.d(TAG, "ALARM JOB SERVICE RUN!");
        mIsPeriodic = params.getExtras().getBoolean(EXTRA_IS_PERIODIC, false);
        EventsHelper.sendReminderProcessAloomaEvent("jobServiceStartRunning: " +
                (mIsPeriodic ? "periodic" : "once"));
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new Task(this, params));

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    class Task implements Runnable {

        Context context;
        JobParameters params;

        private Task(Context context, JobParameters params) {
            this.context = context;
            this.params = params;
        }

        @Override
        public void run() {
            new ActionSetItemAlarm().start(context);
            startJobServicePeriodic(context);

            EventsHelper.sendReminderProcessAloomaEvent("jobServiceFinishRunning: " +
                    (mIsPeriodic ? "periodic" : "once"));
            jobFinished(params, false);
        }
    }
}
