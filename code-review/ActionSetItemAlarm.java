package com.medisafe.android.base.alarms;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.medisafe.android.base.actions.ActionDoContinueGroup;
import com.medisafe.android.base.client.net.response.handlers.UserResponseHandler;
import com.medisafe.android.base.helpers.Config;
import com.medisafe.android.base.helpers.EventsConstants;
import com.medisafe.android.base.helpers.EventsHelper;
import com.medisafe.android.base.helpers.InfectingAppsHelper;
import com.medisafe.android.base.helpers.RemoteLog;
import com.medisafe.android.base.helpers.StyleHelper;
import com.medisafe.android.base.helpers.UIHelper;
import com.medisafe.android.base.managerobjects.MedisafeAlarmManager;
import com.medisafe.android.base.recievers.RepeatAlarmBroadcastReceiver;
import com.medisafe.android.base.service.SchedulingService;
import com.medisafe.android.base.utils.AlarmUtils;
import com.medisafe.android.client.MyApplication;
import com.medisafe.android.client.alooma.AloomaWrapper;
import com.medisafe.common.Mlog;
import com.medisafe.model.DatabaseManager;
import com.medisafe.model.dataobject.ScheduleItem;
import com.medisafe.model.dataobject.User;
import com.medisafe.network.NetworkRequestManager;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.google.zxing.client.android.AlarmService.setAppointmentsAlarmRequestAfterReboot;
import static com.google.zxing.client.android.AlarmService.setRefillAlarmRequestAfterReboot;

public class ActionSetItemAlarm {

    public static final String TAG = ActionSetItemAlarm.class.getSimpleName();

    enum Result {
        SUCCESS, RETRY
    }

    public Result start(Context context) {
        Result result = Result.SUCCESS;
        try {
            MedisafeAlarmManager.setAlarms(context);
        } catch (Exception e) {
            Exception cle = new Exception("MedisafeAlarmWorkerSetAlarmFailed", e);
            Crashlytics.logException(cle);
            EventsHelper.sendReminderProcessAloomaEvent("setAlarmFailed");
            result = Result.RETRY;
        }

        try {
            startContinues(context);
        } catch (Exception e) {
            Exception cle = new Exception("MedisafeAlarmWorkerStartContinuesFailed", e);
            Crashlytics.logException(cle);
            EventsHelper.sendReminderProcessAloomaEvent("startContinuesFailed");
            result = Result.RETRY;
        }

        //run only once a day
        long time = Config.loadLongPref(Config.PREF_KEY_LAST_SERVICE_RUN_TIME, context);
        if (time == -1 || (System.currentTimeMillis() - time) >= DateUtils.DAY_IN_MILLIS) {

            Config.saveLongPref(Config.PREF_KEY_LAST_SERVICE_RUN_TIME, System.currentTimeMillis(),
                    context);
            // Reset events sent pref to enable new day events to be send
            resetEventsSentPrefs(context);
            // retrieve google advertising id
            retrieveGoogleAdvertisingId(context);
        }

        initOtherReminders(context);


        return result;
    }

    /**
     * init alarms e.g refill, appointment etc
     *
     * @param context
     */
    private void initOtherReminders(Context context) {
        Mlog.i(TAG, "initOtherReminders");
        // Set evening alarm
        if (Config.loadEveningReminderPref(context)) {
            AlarmUtils.enableEveningAlarm(context, true);
        }

        if (Config.loadMorningReminderPref(context)) {
            AlarmUtils.enableMorningAlarm(context, true);
        }

        if (Config.loadPositiveFeedbackReminderPref(context)) {
            AlarmUtils.setPositiveFeedbackNotification(context, true, Config
                    .loadPositiveFeedbackReminderHourPref(context));
        }

        setRefillAlarmRequestAfterReboot(context);
        scheduleRepeatAlarm(context);
        scheduleUpdateWidgetAlarm(context);
        setAppointmentsAlarmRequestAfterReboot(context);
    }

    private void startContinues(Context context) {
        boolean schedulingUpgraded = Config.loadBooleanPref(Config.PREF_KEY_SCHEDULING_UPGRADED,
                context);
        Mlog.d(TAG, "schedulingUpgraded = " + schedulingUpgraded);
        long lastContinuesRun = Config.loadLongPref(Config.PREF_KEY_LAST_CONTINUES_DATE,
                context);
        Mlog.d(TAG, "lastContinuesRun = " + lastContinuesRun);
        AloomaWrapper.trackEvent(new AloomaWrapper
                .Builder(EventsConstants.MEDISAFE_EV_CONTINUES_LAST_RUN)
                .setValue(String.valueOf(lastContinuesRun)));

        boolean isLastContinuesDateToday = DateUtils.isToday(lastContinuesRun);
        // continues should run only once a day and only after scheduling has been upgraded
        if (schedulingUpgraded && !isLastContinuesDateToday) {
            Mlog.d(TAG, "calling continues action");
            new ActionDoContinueGroup().start(context);
        } else {
            Mlog.w(TAG, "discarding continues action");
            //for debugging
            boolean cond = Config.loadBooleanPref(Config.PREF_KEY_HAS_CONTINUES_PROBLEM,
                    context);
            AloomaWrapper.trackDebugEvent(new AloomaWrapper.Builder(
                    "continues not running")
                    .setTypeSystem(), cond);
        }
    }

    public static void rescheduleMissedWeekendMeds(Context context) {
        RemoteLog.ri(context, TAG, "start rescheduleMissedWeekendMeds");

        Calendar startDate = Calendar.getInstance();
        startDate.set(Calendar.HOUR_OF_DAY, Config.loadMorningStartHourPref(context));
        startDate.set(Calendar.MINUTE, 0);
        startDate.set(Calendar.SECOND, 0);
        startDate.set(Calendar.MILLISECOND, 0);

        Calendar endDate = Calendar.getInstance();
        endDate.set(Calendar.HOUR_OF_DAY, Config.loadWeekendModeHourPref(context));
        endDate.set(Calendar.MINUTE, 0);
        endDate.set(Calendar.SECOND, 0);
        endDate.set(Calendar.MILLISECOND, 0);

        List<ScheduleItem> items = DatabaseManager.getInstance().
                getPendingScheduleItemsBetweenDatesSorted(startDate.getTime(), endDate.getTime());
        try {
            items = DatabaseManager.getInstance().getScheduleData(items);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (items == null) {
            return;
        }

        SchedulingService.startActionRescheduleItems(context, items,
                endDate.getTime(), null, false);
    }

    private void resetEventsSentPrefs(Context context) {
        Config.saveBooleanPref(Config.PREF_KEY_EVENT_SENT_TAKE, false, context);
        Config.saveBooleanPref(Config.PREF_KEY_EVENT_SENT_SNOOZE, false, context);
        Config.saveBooleanPref(Config.PREF_KEY_EVENT_SENT_SKIP, false, context);
        Config.saveBooleanPref(Config.PREF_KEY_EVENT_SENT_TAKE_ALL, false, context);
        Config.saveBooleanPref(Config.PREF_KEY_EVENT_SENT_SNOOZE_ALL, false, context);
    }

    private void retrieveGoogleAdvertisingId(Context context) {
        // Check default user exist and has a valid token inorder not to enter a request to queue
        // before request token
        User defaultUser = ((MyApplication) context.getApplicationContext()).getDefaultUser();
        if (defaultUser != null && !TextUtils.isEmpty(defaultUser.getAuthToken())) {
            try {
                AdvertisingIdClient.Info info = AdvertisingIdClient.getAdvertisingIdInfo(context);
                String prevId =
                        Config.loadStringPref(Config.PREF_KEY_GOOGLE_ADVERTISING_ID, null,
                                context);

                String id = null;
                if (info != null && !info.isLimitAdTrackingEnabled()) {
                    id = info.getId();
                    Mlog.d(TAG, "retrieve google ad id: " + id + " [prev id = " + prevId + "]");
                }

                Config.saveStringPref(Config.PREF_KEY_GOOGLE_ADVERTISING_ID, id, context);

                boolean isIdUpdated = (!TextUtils.isEmpty(id) && !id.equals(prevId)) ||
                        (!TextUtils.isEmpty(prevId) && TextUtils.isEmpty(id));
                boolean isIdNotExist = prevId == null;

                if (isIdNotExist || isIdUpdated) {
                    Mlog.d(TAG, "send google ad id to server");
                    NetworkRequestManager.UserNro.createUpdateMyUserRequest(context,
                            defaultUser,
                            defaultUser,
                            InfectingAppsHelper.hasInfectingApp(),
                            Config.loadStringPref(Config.PREF_KEY_GOOGLE_ADVERTISING_ID, context),
                            UIHelper.replaceAvatarsChristmasToOrdinary(defaultUser.getImageName()),
                            Config.loadAppVersionPref(context),
                            StyleHelper.getThemeColor(defaultUser.getId()).getColorName(),
                            Locale.getDefault().getLanguage().toLowerCase(),
                            Config.loadLongPref(Config.PREF_KEY_SAFETYNET_JOINED_TIME_STAMP, context),
                            new UserResponseHandler())
                            .dispatchQueued();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void scheduleRepeatAlarm(Context context) {
        try {
            RepeatAlarmBroadcastReceiver.scheduleRepeatAlarm(context);
            Config.saveBooleanPref(Config.PREF_KEY_REPEAT_ALARM_CHECK_SET, true, context);
        } catch (Exception e) {
            Mlog.e(TAG, e.getMessage(), e);
            Crashlytics.logException(e);
        }

    }

    private void scheduleUpdateWidgetAlarm(Context context) {
        try {
            AlarmUtils.setUpdateWidgetAlarm(context);
            Config.saveBooleanPref(Config.PREF_KEY_REPEAT_ALARM_UPDATE_WIDGET, true,
                    context);
        } catch (Exception e) {
            Mlog.e(TAG, e.getMessage(), e);
            Crashlytics.logException(e);
        }
    }
}
